##Syntax##
http://boxstarter.org/package/nr/{package1,package2}
http://boxstarter.org/package/nr/url?{script url}

##Quick Reference URLS##
http://boxstarter.org/package/nr/notepadplusplus,7zip,adobereader,DotNet3.5,DotNet4.0,DotNet4.5,PowerShell,powershell4,pscx,virtualclonedrive,winmerge
http://boxstarter.org/package/nr/GoogleChrome,Firefox,notepadplusplus,7zip,curl,Wget,adobereader,DotNet3.5,DotNet4.0,DotNet4.5,PowerShell,powershell4,pscx
http://boxstarter.org/package/nr/url?http://git.redgila.net/boxstarter/raw/f84776a237803b22be8c997dd351c03b10ac35cd/KH_BASE_HOST.txt
http://boxstarter.org/package/nr/url?http://git.redgila.net/boxstarter/raw/4338454ee80523c2b9d3f1bb29bf926ef7282e4a/virtuallab_w08r2.txt

##Online Documentation##
*http://boxstarter.org/WhyBoxstarter
*http://chocolatey.org/
*https://www.nuget.org/