Set-WindowsExplorerOptions -EnableShowHiddenFilesFoldersDrives -EnableShowFileExtensions
Enable-RemoteDesktop
Disable-InternetExplorerESC
Disable-MicrosoftUpdate
Disable-UAC
 
cinst sysinternals
cinst fiddler4
cinst Firefox 
cinst javaruntime 
cinst GoogleChrome 
cinst notepadplusplus 
cinst 7zip
cinst curl
cinst Wget
cinst tortoisesvn
cinst PowerShell
cinst pscx
cinst flashplayeractivex
cinst flashplayerplugin
cinst adobereader
cinst AdobeAIR
cinst adobeshockwaveplayer
cinst HeidiSQL
cinst SourceTree
cinst DotNet4.0
cinst DotNet4.5
cinst linqpad4
cinst powershell4

